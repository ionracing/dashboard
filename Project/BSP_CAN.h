#ifndef BSP_CAN_H
#define BSP_CAN_H

/* Dependencies -------------------------------------------- */
#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "hardware.h"
/* MACROS ---------------------------------------------------*/
#define BYTE2HALFWORD(MSB,LSB) ( (MSB << 8) | (LSB & 0xff ))
#define MASK_LSB(data) (uint8_t)(data & 0xFF)
#define MASK_MSB(data) (uint8_t)(data >> 8)
/* Check if the can recieve message intterupt occured */
#define IS_MSG_PENDING_FIFO0(CANx) ((CANx->RF0R & CAN_RF0R_FMP0) != (uint32_t)0)
#define IS_MSG_PENDING_FIFO1(CANx) ((CANx->RF1R & CAN_RF1R_FMP1) != (uint32_t)0)

/*Constants -------------------------------------------------*/

/*Public functions ------------------------------------------*/
void BSP_CAN_init(void);


/* CAN BUS MESSAGES -----------------------------------------*/
#define ID_SPEED 123
#define ID_CHANGE_MODE 143
#endif
