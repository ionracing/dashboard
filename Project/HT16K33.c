#include "HT16K33.h"

void HT16K33_init()
{
    GPIO_InitTypeDef GPIO_initStruct;
    I2C_InitTypeDef I2C_InitStruct;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
    
    /* Configure GPIO as alternate function */
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_AF;
    GPIO_initStruct.GPIO_OType  = GPIO_OType_OD;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_UP;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = GPIO_Pin_6 | GPIO_Pin_7; 
    GPIO_Init(GPIOB, &GPIO_initStruct);
    
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1); //SCL
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_I2C1); //SDA

	I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStruct.I2C_ClockSpeed = 100000;
	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStruct.I2C_OwnAddress1 = 0x00;
	I2C_Init(I2C1, &I2C_InitStruct);

	I2C_Cmd(I2C1, ENABLE);  
    
    for(int i = 0;i < 0xFFFFF;i++);
    
    HT16K33_sendCmd(I2C1, 0x21);   // Sets the oscillator on
    // HT16K33_sendCmd(I2C1, HT16K33_ROW/INT_Setup);
    HT16K33_sendCmd(I2C1, 0x81);
    HT16K33_sendCmd(I2C1, HT16K33_CMD_BRIGHTNESS | 15);
    
    
}


void HT16K33_sendCmd(I2C_TypeDef* I2Cx, uint8_t data)
{
    I2C_AcknowledgeConfig(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

    I2C_GenerateSTART(I2Cx, ENABLE);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

    I2C_Send7bitAddress(I2Cx, SLAVEADDR, I2C_Direction_Transmitter);
		while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){};
        
    I2C_SendData(I2Cx, data);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
        
    I2C_GenerateSTOP(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
}

void HT16K33_write(I2C_TypeDef* I2Cx, uint8_t cmdAddr, uint8_t data)
{
    I2C_AcknowledgeConfig(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

    I2C_GenerateSTART(I2Cx, ENABLE);
    while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));
	
    I2C_Send7bitAddress(I2Cx, SLAVEADDR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){};

    I2C_SendData(I2Cx, data);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    if (cmdAddr)
    {    
        I2C_SendData(I2Cx, 0x00);
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    }

    I2C_GenerateSTOP(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
}

void HT16K33_writebuffer(I2C_TypeDef* I2Cx, uint16_t* data, uint8_t length)
{
    I2C_AcknowledgeConfig(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

    I2C_GenerateSTART(I2Cx, ENABLE);
    while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));
	
    I2C_Send7bitAddress(I2Cx, SLAVEADDR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){};

    I2C_SendData(I2Cx, 0);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
        
    for(uint8_t i = 0; i < length; i++)
    {
        I2C_SendData(I2Cx, data[i] & 0xff);
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
        I2C_SendData(I2Cx, data[i] >> 8);
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    }
        

    I2C_GenerateSTOP(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
}
