#ifndef HARDWAREV1_H
#define HARDWAREV1_H

/* Dependencies -----------------------------------*/
#include "stm32f4xx_gpio.h"

/* Pin and port mapping ---------------------------*/
#define GPIO_LED1 GPIOB
#define PIN_LED1 GPIO_Pin_12

#define GPIO_LED2 GPIOB
#define PIN_LED2 GPIO_Pin_13

#define GPIO_LED3 GPIOA
#define PIN_LED3 GPIO_Pin_5

#define GPIO_LED4 GPIOA
#define PIN_LED4 GPIO_Pin_6

#define GPIO_LED5 GPIOA
#define PIN_LED5 GPIO_Pin_7

#define GPIO_LED6 GPIOB
#define PIN_LED6 GPIO_Pin_0

#define GPIO_LED7 GPIOB
#define PIN_LED7 GPIO_Pin_1

//---------------------------------------------------------------

#define GPIO_CAN GPIOB
#define PIN_CAN_TX GPIO_Pin_9
#define PIN_CAN_RX GPIO_Pin_8

#define GPIO_USART GPIOB
#define PIN_USART_TX GPIO_Pin_10
#define PIN_USART_RX GPIO_Pin_11

#define GPIO_I2C GPIOB
#define PIN_I2C_SCL GPIO_Pin_6
#define PIN_I2C_SDA GPIO_Pin_7

/* USART Settings ----------------------------*/
/* I2C Settings ------------------------------*/
/* CAN Settings ------------------------------*/
#define CAN_BAUDRATE 500 /*Kb/s*/
#endif
