#ifndef HT16K33_H
#define HT16K33_H

#include "stdint.h"
#include "stm32f4xx_i2c.h"
#include "hardwareV1.h"

#define SLAVEADDR 0xE0

#define HT16K33_BLINK_CMD 0x80
#define HT16K33_BLINK_DISPLAYON 0x01
#define HT16K33_BLINK_OFF 0
#define HT16K33_BLINK_2HZ  1
#define HT16K33_BLINK_1HZ  2
#define HT16K33_BLINK_HALFHZ  3

#define HT16K33_CMD_BRIGHTNESS 0xE0


void HT16K33_init(void);
/*
void HT16K33_setBrightness(uint8_t b);
void HT16K33_blinkRate(uint8_t b);
void HT16K33_begin(uint8_t addr);
void HT16K33_writeDisplay(void);
void HT16K33_clear(void);
*/
void HT16K33_sendCmd(I2C_TypeDef* I2Cx, uint8_t data);


void HT16K33_write(I2C_TypeDef* I2Cx, uint8_t slaveAdr, uint8_t cmd);
void HT16K33_writebuffer(I2C_TypeDef* I2Cx, uint16_t* data, uint8_t length);

#endif
