/*Function prototypes*/
#include "storage.h"
#include "buffer.h"
#include "BSP_GPIO.h"
#include "BSP_CAN.h"
#include "delay.h"
#include "HT16K33.h"
#include "stm32f4xx.h"

/* Constants -------------------------------------------------------------*/
#define BUFFER_SIZE 32 /* Size of buffer must be a power of 2 */
#define speed 1
#define soc 2
#define temp 3

/* Local variables --------------------------------------------------------*/
uint8_t mode = speed;

/* Circular buffer variables for CAN1 and CAN2 */
circBuffer_t bufferCan1_s;
tElement bufferCan1[BUFFER_SIZE]; /* Buffer for can1 */

static void displaySpeed(uint16_t value);
static void changeMode(uint16_t value);
void HT16K33_writeDisp(uint16_t dig0, uint16_t dig1, uint16_t dig2, uint16_t dig3);
void writeErrorLeds(uint8_t leds);

typedef struct
{
    uint8_t ID;
    void (*execute)(uint16_t);
}msgHandler_t;

msgHandler_t msgHandler[] = 
{
    {ID_SPEED, &displaySpeed},
    {ID_CHANGE_MODE, &changeMode},
    {0,0}
};


uint16_t alphafonttable[] = 
{ 
    0x0001,
    0x0002,
    0x0004,
    0x0008,
    0x0010,
    0x0020,
    0x0040,
    0x0080,
    0x0100,
    0x0200,
    0x0400,
    0x0800,
    0x1000,
    0x2000,
    0x4000,
    0x8000,
    0x0000,
    0x0000,
    0x0000,
    0x0000,
    0x0000,
    0x0000,
    0x0000,
    0x0000,
    0x12C9,
    0x15C0,
    0x12F9,
    0x00E3,
    0x0530,
    0x12C8,
    0x3A00,
    0x1700,
    0x0000, //  
    0x0006, // !
    0x0220, // "
    0x12CE, // #
    0x12ED, // $
    0x0C24, // %
    0x235D, // &
    0x0400, // '
    0x2400, // (
    0x0900, // )
    0x3FC0, // *
    0x12C0, // +
    0x0800, // ,
    0x00C0, // -
    0x0000, // .
    0x0C00, // /
    0x003F, /* 0 */
    0x0006, /* 1 */
    0x005B, /* 2 */
    0x004F, /* 3 */
    0x0066, /* 4 */
    0x006D, /* 5 */
    0x007D, /* 6 */
    0x0007, /* 7 */
    0x007F, /* 8 */
    0x006F, /* 9 */
    0x1200, // :
    0x0A00, // ;
    0x2400, // <
    0x00C8, // =
    0x0900, // >
    0x1083, // ?
    0x02BB, // @
    0x0077, // A 
    0x007C, // B
    0x0039, // C
    0x005E, // D
    0x0079, // E
    0x0071, // F
    0x007D, // G
    0x0076, // H
    0x0006, // I
    0x001E, // J
    0x2470, // K
    0x0038, // L
    0x0536, // M
    0x0037, // N
    0x003F, // O
    0x0073, // P
    0x203F, // Q
    0x0033, // R
    0x006D, // S
    0x0078, // T
    0x003E, // U
    0x0C30, // V
    0x2836, // W
    0x2D00, // X
    0x1500, // Y
    0x0C09, // Z
    0x0039, // [
    0x2100, // 
    0x000F, // ]
    0x0C03, // ^
    0x0008, // _
    0x0100, // `
    0x0077, // a
    0x2078, // b
    0x00D8, // c
    0x088E, // d
    0x0858, // e
    0x0071, // f
    0x048E, // g
    0x1070, // h
    0x1000, // i
    0x000E, // j
    0x3600, // k
    0x0030, // l
    0x10D4, // m
    0x1050, // n
    0x00DC, // o
    0x0170, // p
    0x0486, // q
    0x0050, // r
    0x2088, // s
    0x0078, // t
    0x001C, // u
    0x2004, // v
    0x2814, // w
    0x28C0, // x
    0x200C, // y
    0x0848, // z
    0x0949, // {
    0x1200, // |
    0x2489, // }
    0x0520, // ~
    0x3FFF,
};

int main(void)
{   
    
    /* Initialize system */
    
    //CanRxMsg can1rxmsg;
    //systickInit(1000); /* delay_ms(10) */
    BSP_GPIOinitDigOut(GPIO_LED1, PIN_LED1);
    BSP_GPIOinitDigOut(GPIO_LED2, PIN_LED2);
    BSP_GPIOinitDigOut(GPIO_LED3, PIN_LED3);
    BSP_GPIOinitDigOut(GPIO_LED4, PIN_LED4);
    BSP_GPIOinitDigOut(GPIO_LED5, PIN_LED5);
    BSP_GPIOinitDigOut(GPIO_LED6, PIN_LED6);
    BSP_GPIOinitDigOut(GPIO_LED7, PIN_LED7);
    BSP_CAN_init();
    
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_4); 			
	IWDG_SetReload(0xFFF);
    
    HT16K33_init();
    
    for(int i = 0;i < 0xFFFFF;i++);
    
    HT16K33_writeDisp(alphafonttable['8'], alphafonttable['8'], alphafonttable['8'], alphafonttable['8']);
    
    writeErrorLeds(0x1F);
    
    for(int i = 0;i < 0x1FFFFFF;i++);
    
    writeErrorLeds(0x00);
    
    HT16K33_writeDisp(0, 0, 0, 0);
    
    for(int i = 0;i < 0xFFFFFF;i++);
    
    HT16K33_writeDisp(alphafonttable['I'], alphafonttable['O'], alphafonttable['N'], 0);
    
    for(int i = 0;i < 0x3FFFFFF;i++);
    
    HT16K33_writeDisp(0, 0, 0, 0);
    
    for(int i = 0;i < 0xFFFFFF;i++);
    
    IWDG_Enable();
    while(1)
    {
        IWDG_ReloadCounter();
    }
}

void HT16K33_writeDisp(uint16_t dig0, uint16_t dig1, uint16_t dig2, uint16_t dig3)
{
    uint16_t data[8];
    
    data[0] = dig0;
    data[1] = dig1;
    data[2] = 0;
    data[3] = dig2;
    data[4] = dig3;
    data[5] = 0;
    data[6] = 0;
    data[7] = 0;
            
    HT16K33_writebuffer(I2C1, data, 8);
}

void writeErrorLeds(uint8_t leds)
{
    if((leds & (1 << 0)) == (1 << 0))
        GPIO_SetBits(GPIO_LED3, PIN_LED3);
    else
        GPIO_ResetBits(GPIO_LED3, PIN_LED3);
    
    if((leds     & (1 << 1)) == (1 << 1))
        GPIO_SetBits(GPIO_LED4, PIN_LED4);
    else
        GPIO_ResetBits(GPIO_LED4, PIN_LED4);
    
    if((leds & (1 << 2)) == (1 << 2))
        GPIO_SetBits(GPIO_LED5, PIN_LED5);
    else
        GPIO_ResetBits(GPIO_LED5, PIN_LED5);
    
    if((leds & (1 << 3)) == (1 << 3))
        GPIO_SetBits(GPIO_LED6, PIN_LED6);
    else
        GPIO_ResetBits(GPIO_LED6, PIN_LED6);
    
    if((leds & (1 << 4)) == (1 << 4))
        GPIO_SetBits(GPIO_LED7, PIN_LED7);
    else
        GPIO_ResetBits(GPIO_LED7, PIN_LED7);
}


/* Interrupt handlers -----------------------------------------------------------*/



/**
* @brief : IRQ-handler for message recieved on CAN1
* @note : CAN1 is configured to recieve messages on FIFO0
*/
void CAN1_RX0_IRQHandler(void)
{
    __disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
        CanRxMsg msg;
        CAN_Receive(CAN1, 0, &msg);
        
        if(msg.StdId == 0x190)
        {
            HT16K33_writeDisp((msg.Data[1] << 8) | msg.Data[0], (msg.Data[3] << 8) | msg.Data[2], (msg.Data[5] << 8) | msg.Data[4], (msg.Data[7] << 8) | msg.Data[6]);
        }
        if(msg.StdId == 0x191)
        {
            writeErrorLeds(msg.Data[0]);
        }
	}
	__enable_irq();
}

static void displaySpeed(uint16_t value)
{
    
}

static void changeMode(uint16_t value)
{
    
}

