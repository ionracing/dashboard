#ifndef HARDWARE_H
#define HARDWARE_H

/* Including warning flag if compiling for a old board. 
#warning "compiling for development board!" 
*/

#if defined COMPILING_FOR_V1
    #include "HardwareV1.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
