#ifndef DISPLAY_H
#define DISPLAY_H

/* Dependencies ----------------------*/
#include "stdint.h"

/* Public variables ------------------*/
/* Public constants/typedefs ---------*/
/* Public functions ------------------*/

/**
* @brief : Initializes the display. Must be
*		called before using the display.
* @input : None.
* @retval : None. 
*/
void display_init(void);

void display_clear(void);

void display_writeNumber(uint16_t number);
#endif
