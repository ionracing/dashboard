/* Includes */
#include "stm32f4xx.h"
#include "storage.h"

/* constants --------------------------------------------------------------*/
#define FLASH_PREAMPLE 0xEF21
#define FLASH_START_ADRESS  0x080E0000

/* Local Variables --------------------------------------------------------*/
storageValues_t storedValues;

/* Private prototypes ---------------------------------------------------- */

/* Macros -----------------------------------------------------------------*/
#define FLASH_ADRESS(k) FLASH_START_ADRESS + (1 + k)*2

/* Public functions -------------------------------------------------------*/

void storage_saveValues(storageValues_t values)
{
    /* Cast struct to a byte array */
    uint16_t *valueArray = (uint16_t*)(&values);
    uint16_t length = (sizeof(values) / sizeof(uint16_t));
    /* Flashed must be unclocked and ereased before it can be written to */
	FLASH_Unlock();
	
    /* Flags must be cleared before the writing process begins */
    FLASH_ClearFlag(
        FLASH_FLAG_EOP |  
        FLASH_FLAG_WRPERR | 
        FLASH_FLAG_PGAERR | 
        FLASH_FLAG_PGPERR | 
        FLASH_FLAG_PGSERR
    );
    FLASH_EraseSector(FLASH_Sector_11, VoltageRange_3); 
    
    /*Save the preamble */
    FLASH_ProgramHalfWord(FLASH_START_ADRESS, FLASH_PREAMPLE);
    /* Save the values */
    uint16_t k = 0;
    for(k = 0; k<=length; k++)
    {
        /*TODO : check if the write was successfull */
         FLASH_ProgramHalfWord(FLASH_ADRESS(k), valueArray[k]);
    }
    /*
    Add endamble to make sure all the data of is stored 
    to prevent loading incomplete dataset if more data is 
    added to storedValues struct than is stored.
    */
    //FLASH_ProgramHalfWord(FLASH_ADRESS(k+1), FLASH_PREAMPLE); 
    /* The flash must be locked again after writing */
    FLASH_Lock();
}


/*TODO : rewrite function to use a pointer to values */

storageValues_t storage_readValues(storageValues_t values)
{
    uint16_t length = (sizeof(values)/sizeof(uint16_t))-1;
    uint16_t valueArray[length];
    
    /* Check if the preamble is there */
    if(*(uint16_t *)(FLASH_START_ADRESS) == FLASH_PREAMPLE)
    {
        for(uint16_t k = 0; k<= length; k++)
        {
            valueArray[k] = *(uint16_t*)(FLASH_ADRESS(k));
        }
        values = *((storageValues_t*)valueArray);
        return values;
    }
    else /* Corrupt or missing data */
    {
        /* Save the current values used instead */
        storage_saveValues(values);
        return values;
    }
}

/* Private functions ----------------------------------------------------- */
